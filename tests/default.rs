use array_linked_list::ArrayLinkedList;

#[test]
fn push_front_fill() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_front(i);
    }

    assert_eq!(array.len(), 10);
    assert!(array.capacity() >= 10);

    for (i, element) in array.iter().rev().enumerate() {
        assert_eq!(*element, i);
    }
}

#[test]
fn push_front_fill_with_capacity() {
    let mut array = ArrayLinkedList::with_capacity(10);

    assert_eq!(array.len(), 0);
    assert_eq!(array.capacity(), 10);

    for i in 0..10 {
        array.push_front(i);
    }

    assert_eq!(array.len(), 10);
    assert_eq!(array.capacity(), 10);

    array.push_front(10);

    assert_eq!(array.len(), 11);
    assert!(array.capacity() >= 11);

    for (i, element) in array.iter().rev().enumerate() {
        assert_eq!(*element, i);
    }
}

#[test]
fn push_back_fill() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_back(i);
    }

    assert_eq!(array.len(), 10);
    assert!(array.capacity() >= 10);

    for (i, element) in array.iter().enumerate() {
        assert_eq!(*element, i);
    }
}

#[test]
fn push_back_fill_with_capacity() {
    let mut array = ArrayLinkedList::with_capacity(10);

    assert_eq!(array.len(), 0);
    assert_eq!(array.capacity(), 10);

    for i in 0..10 {
        array.push_back(i);
    }

    assert_eq!(array.len(), 10);
    assert_eq!(array.capacity(), 10);

    array.push_back(10);

    assert_eq!(array.len(), 11);
    assert!(array.capacity() >= 11);

    for (i, element) in array.iter().enumerate() {
        assert_eq!(*element, i);
    }
}

#[test]
fn remove() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_front(i);
    }

    for i in 0..5 {
        let index = i * 2 + 1;
        assert_eq!(array.remove(index).unwrap(), index);
        assert!(array.remove(index).is_none());
    }

    assert!(array.len() == 5);

    for (i, element) in array.iter().rev().enumerate() {
        assert_eq!(*element, i * 2);
    }

    for (i, element) in array.iter().enumerate() {
        assert_eq!(*element, 8 - i * 2);
    }
}

#[test]
fn replace_front() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_front(i);
    }

    for i in 0..5 {
        let index = i * 2 + 1;
        let value = i * 2 + 10;
        assert_eq!(array.replace_front(index, value).unwrap(), index);
        assert_eq!(array.replace_front(index, value).unwrap(), value);
    }

    assert!(array.len() == 10);

    for (i, element) in array.iter().rev().enumerate() {
        assert_eq!(*element, i * 2);
    }
}

#[test]
fn replace_back() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_back(i);
    }

    for i in 0..5 {
        let index = i * 2 + 1;
        let value = i * 2 + 10;
        assert_eq!(array.replace_back(index, value).unwrap(), index);
        assert_eq!(array.replace_back(index, value).unwrap(), value);
    }

    assert!(array.len() == 10);

    for (i, element) in array.iter().enumerate() {
        assert_eq!(*element, i * 2);
    }
}

#[test]
fn pop_front() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_front(i);
    }

    while let Some(element) = array.pop_front() {
        assert_eq!(element, array.len());
    }

    assert!(array.is_empty());
}

#[test]
fn pop_back() {
    let mut array = ArrayLinkedList::new();
    for i in 0..10 {
        array.push_back(i);
    }

    while let Some(element) = array.pop_back() {
        assert_eq!(element, array.len());
    }

    assert!(array.is_empty());
}
