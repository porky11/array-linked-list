use array_linked_list::ArrayLinkedList;

fn get_default_array() -> ArrayLinkedList<u8> {
    let mut array = ArrayLinkedList::new();

    array.push_front(1);
    array.push_back(2);
    array.push_front(3);
    array.push_back(4);
    array.push_back(5);

    array
}

#[test]
fn test_values() {
    let array = get_default_array();

    let vec: Vec<_> = array.iter().copied().collect();
    assert_eq!(vec, vec![3, 1, 2, 4, 5]);
}

#[test]
fn test_values_after() {
    let array = get_default_array();

    let vec: Vec<_> = array.iter_after(1).copied().collect();
    assert_eq!(vec, vec![4, 5]);
}

#[test]
fn test_values_before() {
    let array = get_default_array();

    let vec: Vec<_> = array.iter_before(1).copied().collect();
    assert_eq!(vec, vec![3, 1]);
}

#[test]
fn test_into_values() {
    let array = get_default_array();

    let vec: Vec<_> = array.into_iter().collect();
    assert_eq!(vec, vec![3, 1, 2, 4, 5]);
}

#[test]
fn test_indexed() {
    let array = get_default_array();

    let vec: Vec<_> = array.indexed().collect();
    assert_eq!(vec, vec![(2, &3), (0, &1), (1, &2), (3, &4), (4, &5)]);
}

#[test]
fn test_indexed_after() {
    let array = get_default_array();

    let vec: Vec<_> = array.indexed_after(1).collect();
    assert_eq!(vec, vec![(3, &4), (4, &5)]);
}

#[test]
fn test_indexed_before() {
    let array = get_default_array();

    let vec: Vec<_> = array.indexed_before(1).collect();
    assert_eq!(vec, vec![(2, &3), (0, &1)]);
}

#[test]
fn test_into_indexed() {
    let array = get_default_array();

    let vec: Vec<_> = array.into_indexed().collect();
    assert_eq!(vec, vec![(2, 3), (0, 1), (1, 2), (3, 4), (4, 5)]);
}

#[test]
fn test_indices() {
    let array = get_default_array();

    let vec: Vec<_> = array.indices().collect();
    assert_eq!(vec, vec![2, 0, 1, 3, 4]);
}

#[test]
fn test_indices_after() {
    let array = get_default_array();

    let vec: Vec<_> = array.indices_after(1).collect();
    assert_eq!(vec, vec![3, 4]);
}

#[test]
fn test_indices_before() {
    let array = get_default_array();

    let vec: Vec<_> = array.indices_before(1).collect();
    assert_eq!(vec, vec![2, 0]);
}

#[test]
fn test_into_indices() {
    let array = get_default_array();

    let vec: Vec<_> = array.into_indices().collect();
    assert_eq!(vec, vec![2, 0, 1, 3, 4]);
}
